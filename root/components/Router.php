<?php

class Router{
    
    private $routes;
    
    public function __construct(){
        $routes_path = ROOT."/config/routes.php";
        $this->routes = include $routes_path;
        
    }
    
    private function getURI(){
        if(!empty($_SERVER["REQUEST_URI"])){
            return trim($_SERVER["REQUEST_URI"],"/");
        }
    }


    
    
    public function run() {
        
        
        $uri = $this->getURI();
        
        if(!isset($_COOKIE["lang"])){
            setcookie("lang","ENG");
        }
        
        foreach ($this->routes as $uripattern => $path) {
            if(preg_match("~$uripattern~", $uri)){
                
                $segments = explode("/",$path);
                
                $controllerName = array_shift($segments)."Controller";
                $controllerName = ucfirst($controllerName);
                
                $actionName = "action".ucfirst(array_shift($segments));
                
                $controllerFile = ROOT."/controllers/".$controllerName.".php";
                
                if(file_exists($controllerFile)){
                    include_once $controllerFile;
                }
                
                $controllerObject = new $controllerName;
                $result = $controllerObject->$actionName();
                if($result != null){
                    break;
                }
            }
        }
    }
}

