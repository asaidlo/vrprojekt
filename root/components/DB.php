<?php

class DB{
    public static function getConnection(){
        
        $paramspath = ROOT."/config/db_params.php";
        $params = include $paramspath;
        
        $dsn = "mysql:host={$params["host"]};port=3306;dbname={$params["dbname"]};charset=utf8";
        $db = new PDO($dsn,$params["user"],$params["password"]);
        
        return $db;
    }
}

