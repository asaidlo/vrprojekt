<?php

class Register{
	
	//Funktsioon loob andmebaasi protseduuri abil argumentide põhjal andmebaasi uue kasutaja
	public static function createUser($name, $mail, $password, $hint, $role){
		
		$db = DB::getConnection();
                $sql = 'CALL addUser(:n, :m, :p, :h, :r)';
                $query = $db ->prepare($sql);
		
		$hashed = password_hash( $password, PASSWORD_DEFAULT);
		
		$query ->bindParam(':n', $name);
                $query ->bindParam(':m', $mail);
                $query ->bindParam(':p', $hashed);
		$query ->bindParam(':h', $hint);
		$query ->bindParam(':r',$role);
		
        $query ->execute();
		
	}
      
	//Funktsioon, mis kontrollib, kas argumentidena antud andmed on kasutaja poolt korrektselt sisestatud
	//Tulemusena tagastatakse leitud vigade massiiv
    public static function checkInput($name, $mail, $password,$hint){
        $errors = array();
            
        if(strlen($name)<4){
            $errors[] = "Nimi peab olema vähemalt 3 tähe pikkune";
        }
        if(strlen($password)<6){
            $errors[] = "Parool peab olema vähemalt 6. tähe pikkune";
        }
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL)){
            $errors[] = "E-mail on valesti sisestatud";
        }
        if(!$hint){
            $errors[] = "Sisestage paroolile vihje";
        }
            
        return $errors;
    }
	
	//Funktsioon, mis kontrollib, kas antud meiliaadress on juba kasutuses (meiliadress peab igal kasutajal unikaalne olema)
	public static function checkEmailInUse($mail){
		
		$db = DB::getConnection();
        $sql = 'SELECT * FROM users_view WHERE Email = :m';
        $query = $db ->prepare($sql);
		
		
        $query ->bindParam(':m', $mail);
		
        $query ->execute();
		
		$rows = $query->fetch();
		
		return !empty($rows);
	}
	
}

?>