<?php

class Tasks{
    
    public static function getStudentTasks($email){
        //Siin saab kasutaja ülesandeid ning tagastab neid kontrollerile
        $tasks = array();
        
        if(isset($_COOKIE["tasks"])){
            $db = DB::getConnection();
            $sql = 'SELECT explaining FROM studentTasks WHERE email = :e AND subject = :s';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $email);
            $query->bindParam(':s',$_COOKIE["tasks"]);
            $query->execute();
            
            $i = 0;
            while($row = $query->fetch()){
                $tasks[$i] = $row[0];
                $i++;
            }
        }
        
        
        return $tasks;
    }
    
    public static function getTeacherTasks($email){
        //Siin saab kasutaja ülesandeid ning tagastab neid kontrollerile
        $tasks = array();
        
        if(isset($_COOKIE["tasks"])){
            $db = DB::getConnection();
            $sql = 'SELECT explaining FROM teacherTasks WHERE teacherEmail = :e AND subject = :s';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $email);
            $query->bindParam(':s',$_COOKIE["tasks"]);
            $query->execute();
            
            $i = 0;
            while($row = $query->fetch()){
                $tasks[$i] = $row[0];
                $i++;
            }
        }
        return $tasks;
    }
    
    public static function addTask($mail,$subject,$description){
        $db = DB::getConnection();
        $id = Groups::getGroupId($mail, $subject);
        $sql = 'CALL addTask (:i , :m , :e)';
        $query = $db->prepare($sql);
        $query->bindParam(':e', $description);
        $query->bindParam(':m', $mail);
        $query->bindParam(":i", $id);
        $query->execute();
      
    }
    
}