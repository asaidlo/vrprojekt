<?php

class Groups {
	
	//Funktsioon, mis tagastab andmebaasis leiduvate rühmade info massiivina
    public static function getGroupNames() {
		
        $db = DB::getConnection();
		//Päring on realiseeritud vaatel
        $sql = 'SELECT Subject,Teacher,GroupID FROM groupteachers';
        $query = $db->prepare($sql);

        $query->execute();

        $groupnames = array();

        $i = 0;

        while ($row = $query->fetch()) { //Salvestame iga rea kõik info massiivi

            $groupnames[$i]["teacher"] = $row["Teacher"];

            $groupnames[$i]["group"] = $row["Subject"];

            $groupnames[$i]["ID"] = $row["GroupID"];

            $i++;
        }


        return $groupnames;
    }

	//Funktsioon, mis registreerib antud emailiga kasutaja valitud gruppi
    public static function registerUserToGroup($email, $groupId) {
        $errors = array();

		//Kui kasutaja on juba selles rühmas, siis tagastame vea
        if (Groups::checkIfRegistered($email, $groupId)) {
            $errors[1] = "Te olete juba sellesse rühma registreerunud";
            return $errors;
        }

        $db = DB::getConnection();
		//Andmebaasiga suhtlus toimub protseduuri kaudu
        $sql = 'CALL registerUserToGroup(:e,:i)';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $email);
        $query->bindParam(':i', $groupId);
        $query->execute();


        return $errors;
    }

	//Funktsioon, mis kontrollib, kas antud meiliaadressiga kasutaja on antud rühma juba registreerunud
    public static function checkIfRegistered($email, $groupId) {
        $db = DB::getConnection();
		//SQL päring toimub vaatel
        $sql = 'SELECT * FROM studengregistrations WHERE email = :e AND GroupID = :i';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $email);
        $query->bindParam(':i', $groupId);
        $query->execute();

        $rows = $query->fetch();

        return !empty($rows);
    }

	//Funktsioon andmebaasi uue rühma lisamiseks, argumentidena on antud rühma teema, mahutavus ja juhendaja meiliaadress
    public static function createNewGroup($subject, $capacity, $mail) {

		//Kontrollime sisendeid
        $errors = Groups::checkGroupInputs($subject, $capacity, $mail);

        if (!empty($errors)) {
            return $errors;
        }

        $db = DB::getConnection();
		//Uue rühma loomine toimub protseduuri abil
        $sql = 'CALL createGroup(:s,:c,:m)';
        $query = $db->prepare($sql);

        $query->bindParam(':s', $subject);
        $query->bindParam(':c', $capacity);
        $query->bindParam(':m', $mail);
        $query->execute();


        return $errors;
    }

	//Funktsioon kasutaja sisendite sobivuse kontrolliks rühma loomisel
    public static function checkGroupInputs($name, $subject) {
        $errors = array();


        //TODO: Siia mingisugune sisendite kontroll?

        return $errors;
    }
	
	//Funktsioon, mis tagastab mitmesse rühma antud meiliaadressiga kasutaja registreerunud on
	public static function getNumberOfRegistrations($mail) {
		
		$db = DB::getConnection();
        $sql = 'SELECT COUNT(*) as nr FROM studentregistrations WHERE Email = :e';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $mail);
        $query->execute();
		$row = $query->fetch();
		
		return $row["nr"];
	}
	
	//Funktsioon, mis tagastab mitut rühma antud meiliaadressiga kasutaja juhendab
	public static function getNumberOfTaughtGroups($mail) {
		
		$db = DB::getConnection();
        $sql = 'SELECT COUNT(*) as nr FROM groupteachers WHERE Email = :e';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $mail);
        $query->execute();
		$row = $query->fetch();
		
		return $row["nr"];
	}
        public static function getTeacherGroups($mail){
            	
            $db = DB::getConnection();
            $sql = 'SELECT Subject FROM groupteachers WHERE Email = :e';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $mail);
            $query->execute();
            $groups = array();
            $i = 0;
            
            while($row = $query->fetch()){
                $groups[$i] = $row[0];
                $i++;
            }
		
            return $groups;
        }
        
        public static function getStudentGroups($mail){
            $db = DB::getConnection();
            $sql = 'SELECT Subject FROM studentregistrations WHERE Email = :e';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $mail);
            $query->execute();
            $groups = array();
            $i = 0;
            
            while($row = $query->fetch()){
                $groups[$i] = $row[0];
                $i++;
            }
		
            return $groups;
        }
        
        public static function getGroupId($mail,$subject){
            $db = DB::getConnection();
            $sql = 'SELECT GroupID FROM groupteachers WHERE Email = :e AND subject = :s';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $mail);
            $query->bindParam(':s', $subject);
            $query->execute();
            $row = $query->fetch();
            
            return $row[0];
        }
		
		public static function removeRegistration($student_mail,$group_name, $teacher_mail){
			
			$db = DB::getConnection();
            $sql = 'CALL removeRegistration(:s, :g, :t)';
            $query = $db->prepare($sql);

            $query->bindParam(':s', $student_mail);
            $query->bindParam(':g', $group_name);
            $query->bindParam(':t', $teacher_mail);
			
            $query->execute();

			
			return true;
		}
		
		public static function getGroupStudents($group_name, $teacher_mail){
			
			$db = DB::getConnection();
            $sql = 'SELECT s.Email as mail, s.name as name FROM (studentregistrations as s JOIN groupteachers as g ON g.GroupID = s.GroupID) WHERE g.Email = :e and g.Subject = :n';
            $query = $db->prepare($sql);

            $query->bindParam(':e', $teacher_mail);
            $query->bindParam(':n', $group_name);
			
            $query->execute();

			$result = array();
			
			$i = 0;
			while($row = $query->fetch()){
				$result[$i][0] = $row[0];
				$result[$i][1] = $row[1];
				$i++;
			}
			
			return $result;
		}
}

?>