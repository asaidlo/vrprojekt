<?php

class Login {
	
	//Meetod, mis kontrollib, kas kasutaja sisestas õige parooli
    public static function authoriseUser($email, $password) {

        $db = DB::getConnection();
		//Päring tehakse vaatel
        $sql = 'SELECT phash FROM users_view WHERE email = :e';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $email);
        $query->execute();

        $rows = $query->fetch();
        $phash = $rows[0];
		
		//Sisestatud parooli võrreldakse andmebaasi salvestatud räsiga ning tagastatakse tõeväärtus
        return password_verify($password, $phash); 
    }
	
	//Funktsioon, mis kontrollib, kas antud muutujad on väärtustatud
	//Tulemusena tagastatakse veateadete massiiv
    public static function checkInput($email, $password){
        $errors = array();
        if(empty($email)){
            $errors[] = "Sisestage e-maili";
        }
        if(empty($password)){
            $errors[] = "Sisestage parooli";
        }
        
        return $errors;
    }
	
	//Funktsioon, mis tagastab antud meiliaadressiga kasutaja rolli (õpetaja : "t" või õpilane : "s" )
	public static function getUserRole($email){
		$db = DB::getConnection();
        $sql = 'SELECT Type FROM users_view WHERE email = :e';
        $query = $db->prepare($sql);

        $query->bindParam(':e', $email);
        $query->execute();

        $rows = $query->fetch();

        return $rows[0];
	}
    
	//Funktsioon tagastab antud meiliaadressiga kasutaja nime
	public static function getUserName($email){
            
        $db = DB::getConnection();
        $sql = 'SELECT Displayname FROM users_view WHERE email = :e';
            
        $query = $db->prepare($sql);
        $query->bindParam(':e', $email);
        $query->execute();
        $row = $query->fetch();
     
        return $row[0];
    }
        public static function getUserPicture($email){
        $db = DB::getConnection();
        $sql = 'SELECT picture FROM usersPictures WHERE email = :e';
            
        $query = $db->prepare($sql);
        $query->bindParam(':e', $email);
        $query->execute();
        $row = $query->fetch();
        
        
        return $row[0];
        }
		
	
		
	//Funktsioon Google autentimise jaoks
	public static function authoriseGoogleUser($id){
		include ROOT.'/components/DB.php';
		$db = DB::getConnection();
        $sql = 'SELECT Email,Displayname,Type,picture FROM users_view WHERE googleID = :i';
            
        $query = $db->prepare($sql);
        $query->bindParam(':i', $id);
        $query->execute();
        
		if($row = $query->fetch()){
			return $row;
		}
		return array();
	}

}

?>