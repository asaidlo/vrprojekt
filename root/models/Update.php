<?php

class Update{	
	
	public static function countTeacherStudents($mail){
		
		$db = DB::getConnection();
        $sql = 'SELECT COUNT(DISTINCT s.Email) FROM studentregistrations as s JOIN groupteachers as g ON g.GroupID = s.GroupID WHERE g.Email = :m';
        $query = $db ->prepare($sql);		
		
        $query ->bindParam(':m', $mail);
		
        $query ->execute();		

		return $query->fetch()[0];
	}
	
}

?>