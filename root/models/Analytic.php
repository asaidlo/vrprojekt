<?php



class Analytic{

    
	//Funktsioon uue külastaja andmete salvestamiseks andmebaasi
    public static function newVisitor($date,$ip,$browser){        

        $db = DB::getConnection();

        
		//INSERT on realiseeritud andmebaasi protseduuri kaudu.
        $sql = 'CALL addVisitor (:i, :b, :d)';

        $query = $db ->prepare($sql);

            

        $query ->bindParam(':d', $date);

        $query ->bindParam(':i', $ip);

        $query ->bindParam(':b', $browser);

            

        $query ->execute();

        setcookie("visit","visited", time()+300);

    }

	//Funktsioon, mis kontrollib, kas antud külastaja on juba täna lehekülge külastanud
    public static function checkGuest(){

        

        $date = strval(date("y-m-d",time()));

        $ip = $_SERVER["REMOTE_ADDR"];

        $browser = strval($_SERVER["HTTP_USER_AGENT"]);

        

        $db = DB::getConnection();
        
		//Päring sooritatakse vaatel stat
        $sql = 'SELECT ip FROM stat WHERE ip= :i AND date = :d';        

        $query = $db ->prepare($sql);
            

        $query ->bindParam(':i', $ip);

        $query ->bindParam(':d', $date);

        $query->execute();

        

        $result = $query ->fetch();

		//Kui külastaja on juba täna lehte külastanud, siis suurendame vastavat arvu andmebaasis
        if($result){  

            Analytic::update($ip,$browser);

        }else{
			//Kui tegemist on uue külastajaga, siis lisame ta tabelisse
            Analytic::newVisitor($date, $ip, $browser);

        }

    }

	//Funktsioon, mis suurendab külastaja külastuste arvu andmebaasi tabelis ühe võrra
    public static function update($ip,$browser){

        $db = DB::getConnection();

		//Päring on realiseeritud protseduuri kaudu
        $sql = 'CALL updateVisitor (:b,:i)';

        $query = $db ->prepare($sql);

        $query ->bindParam(':i', $ip);

        $query ->bindParam(':b', $browser);

        $query->execute();

        setcookie("visit","visited", time()+300);

    }

    
	//Funktsioon, mis tagastab andmete massiivina kõik külastajate statistika info andmebaasist
    public static function getData(){

        $db = DB::getConnection();

        $visitors = array();

		//Päring on sooritatud vaatel
        $sql = 'SELECT * FROM stat;';

        $query = $db ->prepare($sql);

        $query->execute();

        $i = 0;

        while($row = $query->fetch()){

            $visitors[$i]["ip"] = $row["ip"];

            $visitors[$i]["date"] = $row["date"];

            $visitors[$i]["browser"] = $row["browser"];

            $visitors[$i]["visits"] = $row["visits"];

            $i++;

        }

        return $visitors;

    }

    

}

?>