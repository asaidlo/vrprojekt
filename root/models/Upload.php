<?php

class Upload{
    
    public static function updatePicture($email,$pictureName){
        $db = DB::getConnection();
        
        $sql = "CALL updatePicture (:e, :p)";
        
        $query = $db ->prepare($sql);
        $query->bindParam(':e', $email);
        $query->bindParam(':p', $pictureName);
        
        $query->execute();
    }
    public static function deletePicture($email){
        $db = DB::getConnection();
        
        $sql = "CALL deletePicture (:e)";
        
        $query = $db ->prepare($sql);
        $query->bindParam(':e', $email);
        
        $query->execute();
    }
}
