var doc = document;


function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(document).on("click",".language",function(){
   doc.cookie = "lang="+$(this).text();
   location.reload();
});

$(document).ready(function(){
    if(getCookie("lang")=="EST"){
        $("html").attr("lang","EST"); 
        $(".language[alt=Eestikeel]").css("background-color","gainsboro");
    }else if(getCookie("lang")=="ENG"){
        $("html").attr("lang","ENG");
        $(".language[alt=English]").css("background-color","gainsboro");
    }
});


