function onSignIn(googleUser) {
	var id_token = googleUser.getAuthResponse().id_token;
		
	$.post('https://testlanguagepractice.000webhostapp.com/login', {idtoken: id_token}, function(response) {
		var destination = getCookie("dir");
		if(getCookie("login_user") != ""){
			if(destination != ""){
				window.location.href = 'https://testlanguagepractice.000webhostapp.com/'+destination;
			}
			else{
				window.location.href = 'https://testlanguagepractice.000webhostapp.com/profile';
			}
		}
		else {
			window.location.href = 'https://testlanguagepractice.000webhostapp.com/login';
		}
		

	});
		
	//Pärast autentimist pole tegelikult google sisse logitud olekut vaja
	signOut();
	
}

function signOut() {
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('User signed out.');
    });
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}