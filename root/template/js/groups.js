
function showStudents(doc){
	var groupname = doc.getAttribute('data-arg1');
	var language = document.getElementById('studentTable').getAttribute('lang');
	if(language === 'EST'){
		var heading1 = 'Õpilase nimi';
		var heading2 = 'Õpilase meil';
		var buttontext = 'Eemalda';
	}

	if(language === 'ENG'){
				var heading1 = 'Student name';
		var heading2 = 'Student email';
		var buttontext = 'Remove';
	}


	$.post('https://testlanguagepractice.000webhostapp.com/groups', {groupname: groupname}, function(response) {

		var students = JSON.parse(response);
		var table = '<table id = "table1"><tr><th>'+heading1+'</th><th>'+heading2+'</th></tr>';
		
		for (var i = 0; i < students.length; i++) {
			row = students[i];
			table = table+ '<tr id = "'+row[0] +'"><td>'+row[1]+"</td><td>"+row[0]+'</td><td><button onClick="removeStudent(this)" data-arg1 = "'+groupname + '" data-arg2 = "'+row[0]+'">'+buttontext+'</a></td></tr>';
		}
		
		document.getElementById("studentTable").innerHTML = table+'</table>';
		
	});	
}


function removeStudent(doc){
	var student = doc.getAttribute("data-arg2");
	var groupname = doc.getAttribute("data-arg1");

	$.post('https://testlanguagepractice.000webhostapp.com/groups', 
	{removestudent: student, groupname : groupname}, function(response) {
		document.getElementById(student).innerHTML = "";	
	});	
}