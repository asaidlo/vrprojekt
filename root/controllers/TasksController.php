<?php

class TasksController{
    
	//See klass reageerib sündmustele ülesannete lahendamise vaatel
    public function actionTasks(){
        
        
        $groups = array();
		//Kui kasutaja pole sisse logitud, siis suuname ta ümber
        if(empty($_COOKIE["login_user"])){
            setcookie("dir", $_SERVER["REQUEST_URI"]);
            header("Location: /login");
        }
        else{
            //Saab kasutaja ülesandeid, millest sõltub Tasks vaade
            if($_COOKIE["role"]=="s"){
                $groups = Groups::getStudentGroups($_COOKIE["login_user"]);
                $tasks = Tasks::getStudentTasks($_COOKIE["login_user"]);
            }else if($_COOKIE["role"]=="t"){
                if(isset($_POST["description"])){
                    Tasks::addTask($_COOKIE["login_user"], $_COOKIE["tasks"], $_POST["description"]);
                }
                $groups = Groups::getTeacherGroups($_COOKIE["login_user"]);
                $tasks = Tasks::getTeacherTasks($_COOKIE["login_user"]);
            }
            unset($_COOKIE["dir"]);
            require_once ROOT.'/views/tasks.php';
        }
        return true;
    }
}

?>