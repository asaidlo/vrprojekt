<?php

class StatisticsController{
    
	//See klass tagab, et statistikavaade töötaks korrektselt
    public function actionStatistics(){
        
		//Küsime andmebaasist kõik statistikaandmed
        $visitors = Analytic::getData();
        
        require_once ROOT.'/views/statistics.php';
        return true;
    }
}