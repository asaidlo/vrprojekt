<?php

class LoginController {
	
	//See klass reageerib login vaatel toimuvatele sündmustele
    public function actionLogin() {
        
        $errors = array();
        
		//Kui kasutaja on midagi vormi kaudu sisestanud
        if (!empty($_POST)) {
            if(!empty($_POST["idtoken"])){
				include ROOT.'/google/vendor/autoloadGoogle.php';
				$client = new Google_Client(['client_id' => "1020545530500-01hde6ol13ut2bmgobtf6lnti0blarbm.apps.googleusercontent.com"]);  // Specify the CLIENT_ID of the app that accesses the backend
				$payload = $client->verifyIdToken($_POST["idtoken"]);
				if ($payload) {	
					include ROOT.'/models/Login.php';
					$userinfo = Login::authoriseGoogleUser($payload["sub"]);
					if(!empty($userinfo)){
						setcookie("login_user",$userinfo["Email"]);
						setcookie("role", $userinfo["Type"]);
						setcookie("name", $userinfo["Displayname"]);
						setcookie("picture",$userinfo["picture"]);
						
					} else{
						$errors[] = "Sellise Google'i kontoga pole ükski kasutaja seotud.";
					}
				}else{
					$errors[] = "Vigane autentimine, palun proovige uuesti.";
				}
			}
			else{
				//Kontrollime väljade täitmise korrektsust
				
				$errors = Login::checkInput($_POST["mail"], $_POST["password"]);
				
				//Kui vigu ei esinenud
				if(empty($errors)) {
				//Autendime kasutaja tema sisestatud parooli abil ning seame küpsise, mis antud tema kohta vajalikku infot hoiab
					if (Login::authoriseUser($_POST["mail"], $_POST["password"])) {
						setcookie("login_user",$_POST["mail"]);
						setcookie("role", Login::getUserRole($_POST["mail"]));
						setcookie("name", Login::getUserName($_POST["mail"]));
						setcookie("picture",Login::getUserPicture($_POST["mail"]));
						
						if(empty($_COOKIE["dir"])){
							header('Location: /profile');
						}else{
							header('Location: '.$_COOKIE["dir"]);
						}
					} else {
						$errors[] = "Vale parool ja/või kasutajanimi";
					}
				}
			}
        }

        require_once ROOT . '/views/login.php';

        return true;
    }
	
	//Funktsioon, mis logib kasutaja välja
    public function actionLogout(){
        setcookie("login_user","",time()-100);
        setcookie("role","",time()-100);
        setcookie("name","",time()-100);
        setcookie("picture","", time()-100);
        unset($groups_nr);
        header("Location: /");
        return true;
    }
}
?>