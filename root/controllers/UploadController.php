<?php

class UploadController{
   
	//See kontroller haldab failide üles laadimist
    public function actionUpload(){
        if(isset($_FILES['fileToUpload'])){
            
            $uploaddir = ROOT."/template/assets/";
            $file_name = $_FILES['fileToUpload']['name'];
            $file_size =$_FILES['fileToUpload']['size'];
            
            $uploadfile = $uploaddir.basename($file_name);
           
            move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$uploadfile);
            //muudab profiilipilti andmebaasis
            Upload::updatePicture($_COOKIE["login_user"], $file_name);
            setcookie("picture",$file_name);
            
         }
        header("Location: /profile");
        
        return true;
    }
    public function actionDelete(){
        $file = ROOT."/template/assets/".$_COOKIE["picture"];
        unlink($file);
        setcookie("picture","", time()-100);
        Upload::deletePicture($_COOKIE["login_user"]);
        header("Location: /profile");
        
        return true;
    }
}