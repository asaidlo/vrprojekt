<?php

class GroupsController {

	//See kontroller reageerib rühmade vaatel toimuvatele tegevustele
    public function actionGroups() {
        $groupnames = array();
        $errors = array();
		
		//Kui kasutaja pole sisse logitud, siis suuname ta ümber login lehele
        if (empty($_COOKIE["login_user"])) {
			//Peame ka meeles, mis lehelt me ta ümber suunasime
            setcookie("dir",$_SERVER["REQUEST_URI"]);
            header("Location: /login");
        }
		
		//Hangime andmebaasist õpilase rühmade arvu (või õppejõu juhendatavate rühmade arvu)
		if(!isset($groups_nr)) {
			if($_COOKIE["role"] == 's') {
				$groups_nr = Groups::getNumberOfRegistrations($_COOKIE["login_user"]);
                                $groups = Groups::getStudentGroups($_COOKIE["login_user"]);
			}
			else if($_COOKIE["role"] == 't') {
				$groups_nr = Groups::getNumberOfTaughtGroups($_COOKIE["login_user"]);
                                $groups = Groups::getTeacherGroups($_COOKIE["login_user"]);
			}
		}

		//Hangime andmebaasist olemasolevate rühmade andmed
        if (empty($groupnames)) {
            $groupnames = Groups::getGroupNames();
        }

		//Kui kasutaja on lehel mingi vormi täitnud, siis reageerime sellele
        if (!empty($_POST)) {
			
            if (!empty($_POST["group"])) { //Rühma registreerumine
                $errors = Groups::registerUserToGroup($_COOKIE["login_user"], $_POST["group"]);
				if(empty($errors)) {
					$groups_nr = $groups_nr + 1;
				}
            }
            if (!empty($_POST["subject"])) { //Uue rühma loomine
                $errors = Groups::createNewGroup($_POST["subject"], $_POST["capacity"], $_COOKIE["login_user"]);
				if(empty($errors)) {
					$groups_nr = $groups_nr + 1;
				}
            }
			
			if (!empty($_POST["removestudent"])) { //Registreeringu eemaldamine
				Groups::removeRegistration($_POST["removestudent"],$_POST["groupname"], $_COOKIE["login_user"]);
				return true;
            }
			
			else if (!empty($_POST["groupname"])){
				echo json_encode(Groups::getGroupStudents($_POST["groupname"], $_COOKIE["login_user"]));
				return true;
			}
        }

        require_once ROOT . '/views/groups.php';
        return true;
    }

}

?>