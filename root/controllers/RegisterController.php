<?php

class RegisterController{
    
	//See klass reageerib sündmustele registreerumisvaates
    public function actionRegister(){
        
        $errors = array();
        
		//Kui kasutaja on midagi vormi kaudu sisestanud
		if (!empty($_POST)){
            
			//Kõigepealt kontrollime kasutaja sisendite õigsust
			$errors = Register::checkInput($_POST["name"],$_POST["mail"],$_POST["password"],$_POST["hint"]);            
			if(empty($_POST["name"]) or empty($_POST["mail"]) or empty($_POST["password"])){
				$errors[] = "Kõik väljad peavad olema täidetud.";
			}
			//Kontrollime, kas antud meiliaadress on juba kasutuses
			else if(Register::checkEmailInUse($_POST["mail"])){
				$errors[] = "See meiliaadress on juba kasutuses";
			}
			//Kui vigu ei olnud, siis loome uue kasutaja ning logime kasutaja sisse, kasutades küpsiseid
			else if(empty($errors)){
				Register::createUser($_POST["name"],$_POST["mail"],$_POST["password"],$_POST["hint"], $_POST["role"]);
				if($_COOKIE["lang"] == "EST") {
					$msg = wordwrap("Tere ".$_POST["name"]."!\nOlete edukalt registreerunud meie keskkonda.\nTeie paroolivihje on: ".$_POST["hint"]);
				}else{
					$msg = wordwrap("Hello ".$_POST["name"]+"!\nYou have successfully registered to our platform.\nYour password hint is: ".$_POST["hint"]);
				}
				mail($_POST["mail"],"Registreerumine",$msg);
				setcookie("name", $_POST["name"]);
				setcookie("login_user",$_POST["mail"]);
				setcookie("role",$_POST["role"]);
				header('Location: /profile');
			}
        }
            	
        require_once ROOT.'/views/register.php';
        
        return true;
    }
    
}




?>
