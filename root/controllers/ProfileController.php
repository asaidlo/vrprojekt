<?php

class ProfileController{
    
	//See klass reageerib sündmustele profiilivaatel
    public function actionProfile(){
        
		//Kui kasutaja pole sisse logitud, siis suuname ta vastavale lehele ümber
        if(empty($_COOKIE["login_user"])){
			//Peame ka meeles, mis lehelt me kasutaja ümber suunasime
            setcookie("dir",$_SERVER["REQUEST_URI"]);
            header("Location: /login");
        }
        else{
            //Saab kasutaja andmed, millest sõltub Profiili vaade
            
            unset($_COOKIE["dir"]);
            require_once ROOT.'/views/profile.php';
        }
        
        
        
        return true;
    }
    
}



?>
