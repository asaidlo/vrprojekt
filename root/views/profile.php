<!DOCTYPE html>

<html>
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Profile</title>
        <meta name="keywords" content="profile name role" />
        <meta name="description" content="User profile">
        <link href="/template/css/profile.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />
        <script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery-1.8.0.min.js">\x3C/script>');</script>
        <script src="/template/js/loading.js"></script>
        <script src="/template/js/language.js"></script>
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        
        <div id="wrapper">
        <?php if($_COOKIE["lang"]=="EST"):?>
        <p class="label">Profiil</p>
        <?php endif;?>
        <?php if($_COOKIE["lang"]=="ENG"):?>
        <p class="label">Profile</p>
        <?php endif;?>        
        <div id="avatar">
            <div id="pilt" data-image="/template/assets/<?php if(isset($_COOKIE["picture"])){
                                                                            echo $_COOKIE["picture"];}
                                                                        else{
                                                                            echo "unknown.jpg";    
                                                                        };
                                                                        ?>"></div>
            <form action="/upload" id="uploadform" method = "POST" enctype="multipart/form-data">
                <br>
                <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                <br>
                <input type="file" name="fileToUpload" accept="image/jpeg" id="fileToUpload" value="Vali pilt"><br>
                <button type="submit" name="submit" class="button">
                    <?php if($_COOKIE["lang"]=="EST"):?>
                          Laadi pilt
                    <?php endif;?>
                    <?php if($_COOKIE["lang"]=="ENG"): ?>
                          Upload image
                    <?php endif; ?>
                </button>
            </form>
            <form action="/delete" id="deleteform" method="DELETE">
                <button type="submit" class="button">
                    <?php if($_COOKIE["lang"]=="EST"):?>
                          Kustuta
                    <?php endif;?>
                    <?php if($_COOKIE["lang"]=="ENG"): ?>
                          Delete
                    <?php endif; ?>
                </button>
            </form>
        </div>
        <div id="profileinfo">
            <ul id="infolist">
                <?php if($_COOKIE["lang"]=="EST"):?>
                    <li class="infoitem">Nimi: <?php echo $_COOKIE["name"]; ?></li>
                    <li class="infoitem">E-mail: <?php echo $_COOKIE["login_user"]; ?></li>
                    <li class="infoitem">Roll: <?php echo $_COOKIE["role"]; ?></li>
                <?php endif;?>
                <?php if($_COOKIE["lang"]=="ENG"): ?>
                    <li class="infoitem">Name: <?php echo $_COOKIE["name"]; ?></li>
                    <li class="infoitem">E-mail: <?php echo $_COOKIE["login_user"]; ?></li>
                    <li class="infoitem">Role: <?php echo $_COOKIE["role"]; ?></li>
                <?php endif; ?>
                
            </ul>
        </div>
        </div>
        
        <?php include ROOT.'/views/footer.php';?>
    </body>
</html>

