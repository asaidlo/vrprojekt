<!DOCTYPE html>

<html>
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Groups</title>
        <meta name="keywords" content="groups create join" />
        <meta name="description" content="All groups that you participate in">
        <link href="/template/css/groups.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />

        <script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery-1.8.0.min.js">\x3C/script>')</script>
        <script src="/template/js/language.js"></script>
        <script src="/template/js/update.js"></script>
		<script src="/template/js/groups.js" type="text/javascript"></script>		
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        <div id="wrapper">
            <?php if($_COOKIE["lang"]=="EST"):?>
                <p class="label">Rühmad</p>
            <?php endif;?>
            <?php if($_COOKIE["lang"]=="ENG"):?>
                <p class="label">Groups</p>
            <?php endif;?> 
		<?php if($_COOKIE["role"] ==  's'): ?>
                    <?php if($_COOKIE["lang"]=="EST"):?>
                <p class="msg">Te olete hetkel <?php echo $groups_nr; ?>-s rühmas.</p>
                    <?php endif;?>
                    <?php if($_COOKIE["lang"]=="ENG"): ?>
                <p class="msg">You are in <?php echo $groups_nr; ?> groups now.<p>
                    <?php endif; ?>
		
		<div class = "placeholder" >
			<label for="registrationform">
                            <?php if($_COOKIE["lang"]=="EST"):?>
                                Registreeru soovitud rühma
                            <?php endif;?>
                            <?php if($_COOKIE["lang"]=="ENG"): ?>
                                Register to a group
                            <?php endif; ?>
                        </label><br><br>
                        <form action="/groups" align="center" id="registrationform" method = "post">
			
			<select name = "group">
			<?php foreach( $groupnames as $group): ?>
			<option value = <?php echo $group["ID"];?>><?php echo $group["group"];?> : <?php echo $group["teacher"]; ?> </option>
			<?php endforeach; ?>
			
			</select><br><br>
			<?php if($_COOKIE["lang"]=="EST"): ?>
			<input class="logininput" type="submit" value="Registreeru">
			<?php else: ?>
			<input class="logininput" type="submit" value="Register">
			<?php endif; ?>
		</form>
		</div>
                <div class="groups">
                    <?php foreach ($groups as $group): ?>
                    <div class="group">
                        <p><?php echo $group; ?></p>
                    </div>
                    <?php endforeach; ?>
                </div>
		<?php endif;?>
		
		<?php if($_COOKIE["role"] ==  't'): ?>
                    <?php if($_COOKIE["lang"]=="EST"):?>
                <p class="msg">Te olete hetkel <?php echo $groups_nr; ?> rühma juhendaja.</p>
                    <?php endif;?>
                    <?php if($_COOKIE["lang"]=="ENG"): ?>
                <p class="msg">Currently you are tutor in <?php echo $groups_nr; ?> groups.</p>
                    <?php endif; ?>
		<?php if($_COOKIE["lang"]=="EST"): ?>
                <p class="msg">Teie rühmadesse on hetkel registreerunud <span id="p1"></span> õpilast.</p>
		<?php else: ?>
                <p class="msg">Currently <span id="p1"></span> students have registered in your groups.</p>
		<?php endif; ?>
		<div class = "placeholder" >
                    <?php if($_COOKIE["lang"]=="EST"):?>
                        <h3>Loo uus rühm</h3>
                        <form action="/groups" align="center" id="createGroup" method = "post">
			
			<label for="subject">Rühma teema: </label>
			<input type="text" name="subject" id="subject" value=""><br>
			<label for="capacity">Rühma suurus: </label>
                    <?php endif;?>
                    <?php if($_COOKIE["lang"]=="ENG"): ?>
                        <h3>Create a new group</h3>
                        <form action="/groups" align="center" id="createGroup" method = "post">
			
			<label for="subject">Group topic: </label>
			<input type="text" name="subject" id="subject" value=""><br>
			<label for="capacity">Group capacity: </label>  
                    <?php endif; ?>
			
			<input type="number" name="capacity" value=""><br>

			<br><br>
			<?php if($_COOKIE["lang"]=="EST"): ?>
			<input type="submit" value="Loo rühm">
			<?php else: ?>
			<input type="submit" value="Create group">
			<?php endif; ?>

		</form>
		</div>
                <div class="groups">
                    <?php foreach ($groups as $group): ?>
                    <div class="group">
						<button data-arg1 = '<?php echo $group ; ?>' type = 'button'  onClick = "showStudents(this)"><?php echo $group ; ?></button>
					</div>
                    <?php endforeach; ?>
					<?php if($_COOKIE["lang"]=="EST"):?>
					<p class="msg">Valige rühm, et näha sinna registreerunud tudengeid.</p>
					<?php endif;?>
					<?php if($_COOKIE["lang"]=="ENG"):?>
					<p class="msg">Select a group to see its registered students.</p>
					<?php endif;?>

					<div id = "studentTable" lang = "<?php echo $_COOKIE["lang"]?>">
						
					</div>
                </div>
				
		<?php endif; ?>
        </div>
		
		
        
        <?php include ROOT.'/views/footer.php';?>
     
    </body>
</html>