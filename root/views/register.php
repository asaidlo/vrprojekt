<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Register</title>
        <meta name="keywords" content="register name password hint mail" />
        <meta name="description" content="Register to Language Practice">
        <link href="/template/css/register.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />
        <script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery-1.8.0.min.js">\x3C/script>')</script>
        <script src="/template/js/language.js"></script>
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        
        
        <?php if(!empty($errors)): ?>
        <div id="errorwrapper">
        <ul id="errors">
            <?php foreach ($errors as $error): ?>
                <li class="error">- <?php echo $error?></li>
            <?php endforeach;?>
            </ul>
        </div>
        <?php
        unset($_POST);
        endif; ?>
                
        <form id="registerform"  method = "post" >
		<?php if($_COOKIE["lang"]=="EST"):?>
                    <label for="name">Nimi:</label>
                    <input class="registerinput" type="text" id="name" name="name" value="" >
                    <div class="help" data-title="Sinu nimi"></div><br>
			<label for="mail">Meiliaadress:</label>
                    <input class="registerinput" type="text" id="mail" name="mail" value="" >
                    <div class="help" data-title="example@test.com"></div><br>
			<label for="password">Parool:</label>
                    <input class="registerinput" type="password" id="password" name="password" value="" >
                    <div class="help" data-title="Vähemalt 6 sümboli"></div><br>
			<label for="hint">Paroolivihje:</label>
                    <input class="registerinput" type="text" id="hint" name="hint" value="" >
                    <div class="help" data-title="Suvaline vihje"></div><br>
			<fieldset>
			<legend>Kasutaja tüüp</legend>
                    <input class="registerinput" type="radio" id="role1" name="role" value="s" checked>
			<label for="role1"> Õpilane</label><br>
                    <input class="registerinput" type="radio" id="role2" name="role" value="t">
			<label for="role2"> Õpetaja</label><br>
			</fieldset>
                <?php endif;?>
                <?php if($_COOKIE["lang"]=="ENG"): ?>
                    <label for="name">Name:</label>
                    <input class="registerinput" type="text" id="name" name="name" value="" >
                    <div class="help" data-title="Sinu nimi"></div><br>
			<label for="mail">E-Mail:</label>
                    <input class="registerinput" type="text" id="mail" name="mail" value="" >
                    <div class="help" data-title="example@test.com"></div><br>
			<label for="password">Password:</label>
                    <input class="registerinput" type="password" id="password" name="password" value="" >
                    <div class="help" data-title="Vähemalt 6 sümboli"></div><br>
			<label for="hint">Hint:</label>
                    <input class="registerinput" type="text" id="hint" name="hint" value="" >
                    <div class="help" data-title="Suvaline vihje"></div><br>
			<fieldset>
			<legend>Role</legend>
                    <input class="registerinput" type="radio" id="role1" name="role" value="s" checked>
			<label for="role1"> Student</label><br>
                    <input class="registerinput" type="radio" id="role2" name="role" value="t">
			<label for="role2"> Teacher</label><br>
			</fieldset>
                <?php endif; ?>
			

            <br>
            <br>
            <input type="submit" value="Registreeru"><br>         
            </form>
        
        <?php include ROOT.'/views/footer.php';?>
    </body>
</html>


