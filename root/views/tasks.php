<!DOCTYPE html>

<html>
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Tasks</title>
        <meta name="keywords" content="tasks groups" />
        <meta name="description" content="All tasks that user have">
        <link href="/template/css/tasks.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />
        <script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery-1.8.0.min.js">\x3C/script>')</script>
        <script src="/template/js/language.js"></script>
        
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        <div id="wrapper">
        <?php if($_COOKIE["lang"]=="EST"):?>
            <p class="label">Ülesanded</p>
        <?php endif;?>
        <?php if($_COOKIE["lang"]=="ENG"):?>
            <p class="label">Tasks</p>
        <?php endif;?> 
        <div id="groups">
            <label>Rühm</label>
            <select class="selectGroup">
                <?php foreach ($groups as $group): ?>
                    <option><?php echo $group;?></option>
                <?php endforeach;?>
            </select>
        </div>
        <?php if($_COOKIE["role"]=="t"):?>
        <div id="newTask">
            <?php if($_COOKIE["lang"]=="ENG"):?>
            <h3>New task</h3>
            <form method="post">
                <label>Description:</label>
                <textarea name="description" rows="6" cols="40"></textarea>
                <input type="submit" value="Add">
            </form>
            <?php endif;?>
            <?php if($_COOKIE["lang"]=="EST"):?>
            <h3>Uus ülesanne</h3>
            <form method="post">
                <label>Kirjeldus:</label>
                <textarea name="description" rows="5" cols="30"></textarea>
                <input type="submit" value="Lisa">
            </form>
            <?php endif;?>
        </div>
        <?php endif;?>
        <div id="tasks">
            <?php foreach ($tasks as $task):?>
                <div class="task">
                    <?php echo $task;?>
                </div>
            
            <?php endforeach; ?>
        </div>
        
        </div>
        
        <?php include ROOT.'/views/footer.php';?>
        <script src="/template/js/tasks.js"></script>
    </body>
</html>
