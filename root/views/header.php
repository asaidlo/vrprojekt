
<header>
    <h1 id="logo">Language Practice</h1>
    <div id="menu">
    <?php if(!isset($_COOKIE["lang"])):?>
        
            <?php if(empty($_COOKIE["use_cookie"])):?>
            <div id="cookieaccept">
                <p>We will use your Cookies</p>
            </div>
            <?php endif; ?>

            <a href="/home" alt="main page">Home</a>
            <?php if(empty($_COOKIE["login_user"])): ?>
            <a href="/login" alt="Login page">Log in</a>
            <a href="/register"alt="Register page">Register</a>
            <?php endif;?>
                <a href="/profile" alt="Users cabinet">Profile</a>
                <a href="/tasks" alt="Users tasks">Tasks</a>
                <a href ="/groups" alt="Users groups">Groups</a>

            <?php if(!empty($_COOKIE["login_user"])): ?>
                <a href="/logout" alt="Log out">Log out</a>
            <?php endif; ?>
            
        
    <?php elseif($_COOKIE["lang"]=="EST"):?>
            <?php if(empty($_COOKIE["use_cookie"])):?>
            <div id="cookieaccept">
                <p>Me kasutame sinu Cookies</p>
            </div>
            <?php endif; ?>
            <a href="/home" alt="Kodu lehekülg">Kodu</a>
            <?php if(empty($_COOKIE["login_user"])): ?>
            <a href="/login" alt="Logimise lehekülg">Login sisse</a>
            <a href="/register" alt="Registreerimise lehekülg">Registreerin</a>
            <?php endif;?>
                <a href="/profile" alt="Isiklik kabinet">Isiklikud andmed</a>
                <a href="/tasks" alt="Kasutaja ülesanded">Ülesanded</a>
                <a href ="/groups" alt="Kasutaja rühmad">Rühmad</a>

            <?php if(!empty($_COOKIE["login_user"])): ?>
                <a href="/logout" alt="Login välja">Login välja</a>
            <?php endif; ?>
            
    <?php elseif($_COOKIE["lang"]=="ENG"):?>
            <?php if(empty($_COOKIE["use_cookie"])):?>
            <div id="cookieaccept">
                <p>We will use your Cookies</p>
            </div>
            <?php endif; ?>
            <a href="/home" alt="main page">Home</a>
            <?php if(empty($_COOKIE["login_user"])): ?>
            <a href="/login" alt="Login page">Log in</a>
            <a href="/register"alt="Register page">Register</a>
            <?php endif;?>
                <a href="/profile" alt="Users cabinet">Profile</a>
                <a href="/tasks" alt="Users tasks">Tasks</a>
                <a href ="/groups" alt="Users groups">Groups</a>

            <?php if(!empty($_COOKIE["login_user"])): ?>
                <a href="/logout" alt="Log out">Log out</a>
            <?php endif; ?>
            
    <?php endif; ?>
                
                
        <div id="languageBox">
            <a class="language" alt="Eestikeel">EST</a>
            <a class="language" alt="English">ENG</a>
        </div>
    </div>
    
    
</header>
