<!DOCTYPE html>

<html>
    <head>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
	<meta name="google-signin-client_id" content="1020545530500-01hde6ol13ut2bmgobtf6lnti0blarbm.apps.googleusercontent.com">
        <title>Login</title>
        <meta name="keywords" content="login email password" />
        <meta name="description" content="Login to Language Practice">
        <link href="/template/css/login.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />
        <script async src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="application/javascript"></script>
	<script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery.min.js" type = "application/javascript">\x3C/script>')</script>
        <script src="/template/js/login.js" type="text/javascript" />
	<script src="https://apis.google.com/js/platform.js" async defer></script>
        <script src="/template/js/language.js"></script>
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
       

        
        <?php if(!empty($errors)): ?>
        <div id="errorwrapper">
        <ul id="errors">
            <?php foreach ($errors as $error): ?>
                <li class="error">- <?php echo $error?></li>
            <?php endforeach;?>
            </ul>
        </div>
        
        <?php 
        unset($_POST);
        endif; ?>
        
        
		
        <form id="loginform" method = "post">
		<?php if($_COOKIE["lang"]=="EST"):?>
                    <label for="mail">Meiliaadress:</label>
                    <input class="logininput" type="text" id="mail" name="mail" value=""><br>
		
                    <label for="password">Parool:</label>
                    <input class="logininput" type="password" id="password" name="password" value=""><br>
                <?php endif;?>
                <?php if($_COOKIE["lang"]=="ENG"): ?>
                    <label for="mail">E-mail:</label>
                    <input class="logininput" type="text" id="mail" name="mail" value=""><br>
		
                    <label for="password">Password:</label>
                    <input class="logininput" type="password" id="password" name="password" value=""><br>
                <?php endif; ?>
		

		<br>
		<input class="logininput" type="submit" value="Sisene">
		</form>
        
		<div class="g-signin2" data-onsuccess="onSignIn"></div>

		

        <?php include ROOT.'/views/footer.php';?>
        
    </body>
</html>
