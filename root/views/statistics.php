<!DOCTYPE html>

<html>
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Language practice</title>
        <link href="/template/css/statistics.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        
        <table id="statisticstable">
            <tr>
                <th>ip</th>
                <th>date</th>
                <th>browser</th>
                <th>visits</th>
            </tr>
        <?php foreach ($visitors as $visitor): ?>
            <tr>
            
            <td> <?php echo $visitor["ip"];?></td> <td><?php echo $visitor["date"];?></td>
            <td><i><?php echo $visitor["browser"];?></i></td><td><?php echo $visitor["visits"]; ?></td>
            
            </tr>
        <?php endforeach; ?>
            
        </table>
    </body>
</html>
