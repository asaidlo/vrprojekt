<!DOCTYPE html>

<html>
    <head>
        <meta content="text/html; charset=UTF-8" name="language practice"/>
        <title>Language practice</title>
        <meta name="keywords" content="Language practice" />
        <meta name="description" content="Language practice main page">
        <link rel="alternate" type="application/atom+xml" title="Language practice est" href="/est"/>
        <link rel="alternate" type="application/atom+xml" title="Language practice eng" href="/eng"/>
        <link href="/template/css/home.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/header.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/footer.css" rel="stylesheet" type="text/css" />
        <script async src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>

        
        <script>window.jQuery || document.write('<script src="<?php echo ROOT."/"; ?>js/jquery-1.8.0.min.js">\x3C/script>');</script>
        <script src="/template/js/language.js"></script>
    </head>
    <body>
        <?php include ROOT.'/views/header.php'; ?>
        
        
       
        
        <?php include ROOT.'/views/footer.php';?>
    </body>
</html>