-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Loomise aeg: Märts 18, 2018 kell 04:52 PL
-- Serveri versioon: 10.2.12-MariaDB
-- PHP versioon: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `id5000594_languagepractice`
--

DELIMITER $$
--
-- Toimingud
--
CREATE DEFINER=`id5000594_epiz_21699178`@`%` PROCEDURE `addUser` (IN `name` VARCHAR(50) CHARSET utf8, IN `mail` VARCHAR(50) CHARSET utf8, IN `password` VARCHAR(255) CHARSET utf8, IN `hint` VARCHAR(50) CHARSET utf8, IN `role` ENUM('s','t') CHARSET utf8)  INSERT INTO Users (Displayname, Email, phash, Hint, Type) VALUES (name,mail,password,hint,role);$$

CREATE DEFINER=`id5000594_epiz_21699178`@`%` PROCEDURE `addVisitor` (IN `ip_p` VARCHAR(30) CHARSET utf8, IN `browser_p` VARCHAR(255) CHARSET utf8, IN `date_p` DATE)  NO SQL
INSERT INTO ips (ip,browser,date,visits) VALUES (ip_p, browser_p,date_p,1)$$

CREATE DEFINER=`id5000594_epiz_21699178`@`%` PROCEDURE `createGroup` (IN `subject_p` VARCHAR(100) CHARSET utf8, IN `capacity` SMALLINT, IN `mail` VARCHAR(50) CHARSET utf8)  NO SQL
INSERT INTO Groups (TeacherID,Subject, MaxCapacity) VALUES ((SELECT ID FROM users_view WHERE Email = mail),subject_p,capacity)$$

CREATE DEFINER=`id5000594_epiz_21699178`@`%` PROCEDURE `registerUserToGroup` (IN `mail` VARCHAR(50) CHARSET utf8, IN `groupId` INT)  NO SQL
INSERT INTO Registrations (GroupID, StudentID) VALUES (groupId,(SELECT id FROM Users WHERE Users.Email = mail))$$

CREATE DEFINER=`id5000594_epiz_21699178`@`%` PROCEDURE `updateVisitor` (IN `browser_p` VARCHAR(255) CHARSET utf8, IN `ip_p` VARCHAR(30) CHARSET utf8)  NO SQL
UPDATE ips SET visits = visits+1,browser = browser_p WHERE ip = ip_p$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `Groups`
--

CREATE TABLE `Groups` (
  `ID` int(11) NOT NULL,
  `TeacherID` int(11) NOT NULL,
  `Subject` varchar(100) CHARACTER SET utf8 NOT NULL,
  `MaxCapacity` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Andmete tõmmistamine tabelile `Groups`
--

INSERT INTO `Groups` (`ID`, `TeacherID`, `Subject`, `MaxCapacity`) VALUES
(2, 19, 'Testimisgrupp', 100),
(4, 19, 'Mingi keel', 10),
(5, 19, 'mingiteema', 27);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `groupteachers`
-- (Tegelik vaade on allpool)
--
CREATE TABLE `groupteachers` (
`Teacher` varchar(50)
,`Email` varchar(50)
,`Subject` varchar(100)
,`GroupID` int(11)
);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `ips`
--

CREATE TABLE `ips` (
  `id` int(12) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `visits` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `ips`
--

INSERT INTO `ips` (`id`, `ip`, `browser`, `date`, `visits`) VALUES
(1, '193.40.12.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', '2018-03-14', 14),
(2, '37.157.98.26', 'Mozilla/5.0 (Linux; Android 8.0.0; ONEPLUS A3003 Build/OPR6.170623.013) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.109 Mobile Safari/537.36', '2018-03-14', 1),
(3, '205.211.169.31', '', '2018-03-14', 12),
(4, '128.30.52.73', 'W3C_Validator/1.3 http://validator.w3.org/services', '2018-03-14', 1),
(5, '128.30.52.95', 'Jigsaw/2.3.0 W3C_CSS_Validator_JFouffa/2.0 (See <http://validator.w3.org/services>)', '2018-03-14', 1),
(6, '66.102.9.130', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-14', 1),
(7, '82.131.127.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-14', 33),
(8, '193.40.13.166', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-15', 13),
(9, '37.157.98.8', 'Mozilla/5.0 (Linux; Android 8.0.0; ONEPLUS A3003 Build/OPR6.170623.013) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.109 Mobile Safari/537.36', '2018-03-15', 1),
(10, '193.40.13.168', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', '2018-03-15', 49),
(11, '66.102.9.153', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-15', 1),
(12, '66.102.9.128', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-15', 1),
(13, '82.131.127.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-15', 14),
(14, '66.102.9.142', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-15', 1),
(15, '193.40.13.168', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', '2018-03-16', 22),
(16, '194.204.56.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-16', 10),
(17, '66.102.9.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-16', 2),
(18, '194.204.33.28', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', '2018-03-16', 1),
(19, '193.40.13.168', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', '2018-03-17', 7),
(20, '66.102.9.152', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-17', 1),
(21, '66.102.9.140', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36 Google Favicon', '2018-03-17', 1),
(22, '193.40.13.168', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36', '2018-03-18', 5),
(41, '128.30.52.134', 'Validator.nu/LV http://validator.w3.org/services', '2018-03-18', 2),
(40, '128.30.52.72', 'Validator.nu/LV http://validator.w3.org/services', '2018-03-18', 2),
(23, '192.69.218.234', 'Browsershots', '2018-03-18', 1),
(24, '192.227.135.12', 'Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0', '2018-03-18', 5),
(25, '93.103.253.31', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20100101 Firefox/13.0', '2018-03-18', 37),
(26, '93.103.253.31', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20100101 Firefox/13.0', '2018-03-18', 37),
(27, '93.103.253.31', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20100101 Firefox/13.0', '2018-03-18', 37),
(32, '198.23.155.190', 'Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0', '2018-03-18', 3),
(28, '195.154.161.152', 'Mozilla/5.0 (X11; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0', '2018-03-18', 33),
(29, '144.76.87.236', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.18 Safari/537.36', '2018-03-18', 38),
(30, '198.23.139.251', 'Mozilla/5.0', '2018-03-18', 9),
(39, '23.94.43.6', 'Mozilla/5.0 (X11; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0', '2018-03-18', 4),
(31, '198.46.157.246', 'Mozilla/5.0 (X11; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0', '2018-03-18', 9),
(33, '198.23.128.219', 'Mozilla/5.0 (X11; Linux x86_64) KHTML/4.8.4 (like Gecko) Konqueror/4.8', '2018-03-18', 3),
(34, '198.46.138.14', 'Mozilla/5.0 (X11; Linux i686 on x86_64; rv:16.0) Gecko/16.0 Firefox/16.0', '2018-03-18', 10),
(35, '75.127.3.7', 'Mozilla/5.0 (X11; Linux x86_64) KHTML/4.8.4 (like Gecko) Konqueror/4.8', '2018-03-18', 6),
(37, '198.23.129.8', 'Mozilla/5.0 (X11; Linux i686 on x86_64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1', '2018-03-18', 7),
(42, '128.30.52.96', 'Validator.nu/LV http://validator.w3.org/services', '2018-03-18', 1),
(36, '198.23.129.8', 'Mozilla/5.0 (X11; Linux i686 on x86_64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1', '2018-03-18', 7),
(38, '198.23.129.8', 'Mozilla/5.0 (X11; Linux i686 on x86_64; rv:7.0.1) Gecko/20100101 Firefox/7.0.1', '2018-03-18', 7);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `Registrations`
--

CREATE TABLE `Registrations` (
  `StudentID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Andmete tõmmistamine tabelile `Registrations`
--

INSERT INTO `Registrations` (`StudentID`, `GroupID`) VALUES
(10, 2),
(17, 2),
(17, 4);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `stat`
-- (Tegelik vaade on allpool)
--
CREATE TABLE `stat` (
`id` int(12)
,`ip` varchar(30)
,`browser` varchar(255)
,`date` date
,`visits` int(100)
);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `studentregistrations`
-- (Tegelik vaade on allpool)
--
CREATE TABLE `studentregistrations` (
`name` varchar(50)
,`Email` varchar(50)
,`GroupID` int(11)
,`Subject` varchar(100)
);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `Users`
--

CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `Displayname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `phash` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Hint` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Type` enum('s','t') CHARACTER SET utf8 NOT NULL DEFAULT 's'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Andmete tõmmistamine tabelile `Users`
--

INSERT INTO `Users` (`ID`, `Displayname`, `Email`, `phash`, `Hint`, `Type`) VALUES
(6, 'Kirill', 'asdas@ff.ee', '$2y$10$SXv/N2AmUGtsQF/Y1b09A.jRO9y7aLfkGZVBKSuxbQfh/sK.yQ.OC', 'ssss', 's'),
(8, 'Carl', 'carl@test.ee', '$2y$10$Ntm7Gw79nUw8FA0j2ngaF.pV0wIt2kFREa5BzGNirwRRa/mNpYszS', 'testcarl', 's'),
(10, 'testkasutaja', 'test@test.ee', '$2y$10$9gcg2oFJZRIgfsC4UP6dAOw50qQ/Phq/QjoK6r197bp9q8nYghN/C', 'parool', 's'),
(11, 'David', 'david@test.ee', '$2y$10$C04Xo6tvD26r/ikVZj8JSutKEeS75euoKMuGHtysMvMYFIWRQLDmi', 'qwerty', 's'),
(12, '<script>alert(\"hehee\")</script>', 'asd@asd.asd', '$2y$10$E7ZoCmsx/ESLN49OLyZ.X..t9OJ3irnI3K1WB4smERycQCcKsUd8m', '<script>alert(\"hehee\")</script>', 's'),
(17, 'Fred', 'fred@test.ee', '$2y$10$JGSOpdH8oWYmkfZ028JWTOIRWZFvdw/gFOkfAifPDjvLiYsstprUi', 'qwerty', 's'),
(18, 'Salme', '123@hot.ee', '$2y$10$zGqRoOxReiGPm.VW4JBkpOxGrr1cjWOOSrWgI5wNA.ZFn70x8fVb2', '654321', 's'),
(19, 'Testõppejõud', 'fake@email.com', '$2y$10$ZwhmpbKzAcu8HWjWZoMlQuSRuq9ZXcGdmaCjCnLIR13OdkySNCDbG', 'õpetaja', 't'),
(20, 'Salme', '123@hot.ee', '$2y$10$4IBeaTcZC5a18STW/g2G4uvE1eC29E3EDBqQk93eefrZ0mxDKPA7i', '654321', 's'),
(21, 'Salme', '123@hot.ee', '$2y$10$wDh5ULBI3FJXBx/BXm8lDOGF8c6ByzymVwz35/kCnggdHkGvzZegK', '654321', 's'),
(22, 'Salme', '123@hot.ee', '$2y$10$g4ZW7FT1W5Pq6BHme3B6YueU2hoj.jNGYHHZHjs9OkV0CxCZLBjpC', '654321', 's'),
(23, 'Salme', '123@hot.ee', '$2y$10$QJArhC2GSgMWXZhzUe7OKuaakpjj5/dJn/CKw77Vr1XJi3GIme2Ia', '654321', 's'),
(24, 'salme', '123@hot.ee', '$2y$10$BVDMwHrafDVZTjv0AajzjOmlbxUcsNeq6P2tbzdvej/4eHSBLheOC', '654321', 's'),
(27, 'ASDKLaskdaskldj', 'sakdjaskdsjd@dlaskda.ee', '$2y$10$ztgF9l4KtEbXfkdnouPMBurPgeH8p5.fgk0UHUUsnLNWszEEZAWia', 'aaaasaaa', 's');

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `users_view`
-- (Tegelik vaade on allpool)
--
CREATE TABLE `users_view` (
`ID` int(11)
,`Displayname` varchar(50)
,`Email` varchar(50)
,`phash` varchar(255)
,`Hint` varchar(50)
,`Type` enum('s','t')
);

-- --------------------------------------------------------

--
-- Vaate struktuur `groupteachers`
--
DROP TABLE IF EXISTS `groupteachers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`id5000594_epiz_21699178`@`%` SQL SECURITY DEFINER VIEW `groupteachers`  AS  select `Users`.`Displayname` AS `Teacher`,`Users`.`Email` AS `Email`,`Groups`.`Subject` AS `Subject`,`Groups`.`ID` AS `GroupID` from (`Users` join `Groups` on(`Users`.`ID` = `Groups`.`TeacherID`)) ;

-- --------------------------------------------------------

--
-- Vaate struktuur `stat`
--
DROP TABLE IF EXISTS `stat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`id5000594_epiz_21699178`@`%` SQL SECURITY DEFINER VIEW `stat`  AS  select `ips`.`id` AS `id`,`ips`.`ip` AS `ip`,`ips`.`browser` AS `browser`,`ips`.`date` AS `date`,`ips`.`visits` AS `visits` from `ips` ;

-- --------------------------------------------------------

--
-- Vaate struktuur `studentregistrations`
--
DROP TABLE IF EXISTS `studentregistrations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`id5000594_epiz_21699178`@`%` SQL SECURITY DEFINER VIEW `studentregistrations`  AS  select `Users`.`Displayname` AS `name`,`Users`.`Email` AS `Email`,`Groups`.`ID` AS `GroupID`,`Groups`.`Subject` AS `Subject` from ((`Users` join `Registrations` on(`Users`.`ID` = `Registrations`.`StudentID`)) join `Groups` on(`Registrations`.`GroupID` = `Groups`.`ID`)) ;

-- --------------------------------------------------------

--
-- Vaate struktuur `users_view`
--
DROP TABLE IF EXISTS `users_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`id5000594_epiz_21699178`@`%` SQL SECURITY DEFINER VIEW `users_view`  AS  select `Users`.`ID` AS `ID`,`Users`.`Displayname` AS `Displayname`,`Users`.`Email` AS `Email`,`Users`.`phash` AS `phash`,`Users`.`Hint` AS `Hint`,`Users`.`Type` AS `Type` from `Users` ;

--
-- Indeksid tõmmistatud tabelitele
--

--
-- Indeksid tabelile `Groups`
--
ALTER TABLE `Groups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TeacherID` (`TeacherID`);

--
-- Indeksid tabelile `ips`
--
ALTER TABLE `ips`
  ADD PRIMARY KEY (`id`);

--
-- Indeksid tabelile `Registrations`
--
ALTER TABLE `Registrations`
  ADD PRIMARY KEY (`StudentID`,`GroupID`),
  ADD KEY `fk_registration_to_groupID` (`GroupID`);

--
-- Indeksid tabelile `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT tõmmistatud tabelitele
--

--
-- AUTO_INCREMENT tabelile `Groups`
--
ALTER TABLE `Groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT tabelile `ips`
--
ALTER TABLE `ips`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT tabelile `Users`
--
ALTER TABLE `Users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Tõmmistatud tabelite piirangud
--

--
-- Piirangud tabelile `Groups`
--
ALTER TABLE `Groups`
  ADD CONSTRAINT `fk_group_to_teacherID` FOREIGN KEY (`TeacherID`) REFERENCES `Users` (`ID`) ON UPDATE CASCADE;

--
-- Piirangud tabelile `Registrations`
--
ALTER TABLE `Registrations`
  ADD CONSTRAINT `fk_registration_to_groupID` FOREIGN KEY (`GroupID`) REFERENCES `Groups` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_registration_to_userID` FOREIGN KEY (`StudentID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
